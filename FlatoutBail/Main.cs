﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Drawing;
using System.IO;
using System.Media;
using System.Windows.Forms;

namespace FlatoutBail
{
    public class FlatoutBail : Script
    {
        const string EJECT_SOUND_LOCATION = "./scripts/FlatoutBail/eject.wav", NUDGE_SOUND_LOCATION = "./scripts/FlatoutBail/nudge.wav", BACKGROUND_IMAGE_LOCATION = "./scripts/FlatoutBail/background.png", NEEDLE_IMAGE_LOCATION = "./scripts/FlatoutBail/needle.png";

        bool enabled, allowNudge, allowMoveRagdoll, playEjectSound, playNudgeSound, controllerSupport;
        Keys toggleKey, bailKey, nudgeKey;
        int bailForce, nudgeForce;
        GTA.Control bailButton, nudgeButton;

        bool showDefaultSettingsMessage = false;

        Point bgPos, needlePos, angleTextPos;
        Size bgSize, needleSize;
        
        UIText angleText;

        int lastUpdateAngle = 0;
        int lastCheckBail = 0;

        bool bailKeyWasPressed = false, inAirAfterBail = false, nudged = false;
        float angle = 0.0f;

        public FlatoutBail()
        {
            string settingsFileName = Filename.Substring(0, Filename.LastIndexOf(".")) + ".ini";

            if (!File.Exists(settingsFileName)) showDefaultSettingsMessage = true;

            bailForce = Settings.GetValue("Settings", "Bail_Force", 100);
            nudgeForce = Settings.GetValue("Settings", "Nudge_Force", 30);
            allowNudge = Settings.GetValue("Settings", "Allow_Nudge", true);
            allowMoveRagdoll = Settings.GetValue("Settings", "Allow_Move_While_In_Air", true);
            enabled = Settings.GetValue("Settings", "Enabled_On_Startup", true);
            playEjectSound = Settings.GetValue("Settings", "Play_Bail_Sound", true);
            playNudgeSound = Settings.GetValue("Settings", "Play_Nudge_Sound", true);

            toggleKey = Settings.GetValue("Controls", "Toggle_Mod_Key", Keys.F9);
            bailKey = Settings.GetValue("Controls", "Bail_Key", Keys.X);
            nudgeKey = Settings.GetValue("Controls", "Nudge_Key", Keys.Space);

            controllerSupport = Settings.GetValue("Controls", "Controller_Support", true);
            bailButton = Settings.GetValue("Controls", "Controller_Bail_Button", GTA.Control.VehicleDuck);
            nudgeButton = Settings.GetValue("Controls", "Controller_Nudge_Button", GTA.Control.Jump);

            Size resolution = Game.ScreenResolution;

            bgPos = new Point((int)(Settings.GetValue("Display", "Background_Pos_X", 0.0025f) * resolution.Width), (int)(Settings.GetValue("Display", "Background_Pos_Y", 0.27f) * resolution.Height));
            needlePos = new Point((int)(Settings.GetValue("Display", "Needle_Pos_X", 0.013f) * resolution.Width), (int)(Settings.GetValue("Display", "Needle_Pos_Y", 0.48f) * resolution.Height));

            bgSize = new Size((int)(Settings.GetValue("Display", "Background_Width", 0.13f) * resolution.Width), (int)(Settings.GetValue("Display", "Background_Height", 0.23f) * resolution.Height));
            needleSize = new Size((int)(Settings.GetValue("Display", "Needle_Width", 0.13f) * resolution.Width), (int)(Settings.GetValue("Display", "Needle_Height", 0.23f) * resolution.Height));

            angleTextPos = new Point((int)(Settings.GetValue("Display", "Angle_Text_Pos_X", 0.005f) * resolution.Width), (int)(Settings.GetValue("Display", "Angle_Text_Pos_Y", 0.49f) * resolution.Height));

            angleText = new UIText("0.0f", angleTextPos, Settings.GetValue("Display", "Angle_Text_Scale", 0.6f), Color.FromName(Settings.GetValue("Display", "Angle_Text_Color", "Orange")));

            try
            {
                AudioPlaybackEngine.Instance.Init();
            }
            catch (NAudio.MmException ex)
            {
                SystemSounds.Beep.Play();
                throw ex;
            }

            if (playEjectSound && !File.Exists(EJECT_SOUND_LOCATION)) playEjectSound = false;

            if (playNudgeSound && !File.Exists(NUDGE_SOUND_LOCATION)) playNudgeSound = false;

            KeyDown += FlatoutBail_KeyDown;
            Interval = 0;
            Tick += FlatoutBail_Tick;
        }

        private void FlatoutBail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == toggleKey)
            {
                enabled = !enabled;
                UI.ShowSubtitle($"Flatout Bail now {(enabled ? "enabled" : "disabled")}");
            }
            else if (enabled)
            {
                if (e.KeyCode == nudgeKey && allowNudge && inAirAfterBail && !nudged)
                {
                    Nudge(Game.Player.Character);
                }
            }
        }

        private void FlatoutBail_Tick(object sender, EventArgs e)
        {
            if (showDefaultSettingsMessage && !Game.IsLoading && !Game.IsScreenFadedOut && !Game.IsScreenFadingIn)
            {
                showDefaultSettingsMessage = false;

                UI.Notify("Flatout High Jump INI file doesn't exist. Using default values (X to eject, F9 to toggle mod).");
            }

            if (!enabled) return;

            if (bailKeyWasPressed)
            {
                float rotation = 0.25f - angle / (90 /* the max angle */ * 4 /* the fraction that the original rotation equals (1/4 = 0.25) */); //Game.Player.Character.CurrentVehicle.Speed * 2.51f * 1.6f / 320.0f + 0.655f;

                UI.DrawTexture(BACKGROUND_IMAGE_LOCATION, 0, 0, 50, bgPos, bgSize);
                UI.DrawTexture(NEEDLE_IMAGE_LOCATION, 1, 1, 50, needlePos, new PointF(0.5f, 0.5f), needleSize, rotation, Color.White, Function.Call<float>(Hash._GET_SCREEN_ASPECT_RATIO, false));

                angleText.Draw();
            }

            int gameTime = Game.GameTime;

            if (bailKeyWasPressed && Game.GameTime > lastUpdateAngle + 5)
            {
                if (angle < 90.0f)
                {
                    angle += 0.5f;
                    angleText.Caption = ((int)angle).ToString();
                }

                lastUpdateAngle = gameTime;
            }

            if (gameTime > lastCheckBail + 50)
            {
                lastCheckBail = gameTime;

                Ped plrPed = Game.Player.Character;

                if (plrPed.Exists() && plrPed.IsAlive)
                {
                    if (inAirAfterBail)
                    {
                        if (Game.Player.Character.HeightAboveGround < 1f || Game.Player.Character.IsInVehicle())
                        {
                            if (allowMoveRagdoll) Game.Player.CanControlRagdoll = false;

                            inAirAfterBail = false;
                            nudged = false;
                        }
                        else
                        {
                            if (Game.IsEnabledControlJustPressed(2, nudgeButton) && !Function.Call<bool>(Hash._GET_LAST_INPUT_METHOD, 2) && allowNudge && !nudged)
                            {
                                Nudge(plrPed);
                            }
                        }
                    }

                    if (plrPed.IsInVehicle())
                    {
                        Vehicle veh = plrPed.CurrentVehicle;

                        if (veh.Exists() && veh.IsAlive && veh.GetPedOnSeat(VehicleSeat.Driver) == plrPed)
                        {
                            bool wasPressed = bailKeyWasPressed;

                            bool pressed = Game.IsKeyPressed(bailKey) ||
                                (controllerSupport && Game.IsEnabledControlPressed(2, bailButton) && !Function.Call<bool>(Hash._GET_LAST_INPUT_METHOD, 2) /* last input was controller */);

                            bailKeyWasPressed = pressed;

                            if (!pressed)
                            {
                                float tempAngle = angle;
                                angle = 0.0f;

                                if (wasPressed) BailOut(plrPed, veh, tempAngle);
                            }
                        }
                        else bailKeyWasPressed = false;
                    }
                    else bailKeyWasPressed = false;
                }
                else inAirAfterBail = nudged = bailKeyWasPressed = false;
            }
        }

        private Vector3 GetModelMaxDimensions(int hash)
        {
            OutputArgument outArgMin = new OutputArgument(), outArgMax = new OutputArgument();
            Function.Call(Hash.GET_MODEL_DIMENSIONS, hash, outArgMin, outArgMax);
            return outArgMax.GetResult<Vector3>();
        }

        private void Nudge(Ped ped)
        {
            nudged = true;

            Game.Player.Character.ApplyForce(new Vector3(0f, 0f, nudgeForce));

            if (playNudgeSound) AudioPlaybackEngine.Instance.PlaySound(NUDGE_SOUND_LOCATION);
        }

        private void BailOut(Ped ped, Vehicle veh, float angle)
        {
            ped.Task.WarpOutOfVehicle(veh);

            for (int i = 0; i < 200; i++)
            {
                if (!ped.IsInVehicle()) break;

                Wait(1);
            }

            if (!ped.IsInVehicle())
            {
                ped.Task.ClearAllImmediately();

                Vector3 dimensions = GetModelMaxDimensions(veh.Model.Hash);

                Vector3 frontPos = veh.GetOffsetInWorldCoords(new Vector3(0f, dimensions.Y + 1f, 0.5f)), upPos = ped.GetOffsetInWorldCoords(new Vector3(0f, 0f, dimensions.Z / 2 + 1f));

                ped.Position = Vector3.Lerp(frontPos, upPos, angle / 90.0f);

                Function.Call<bool>(Hash.SET_PED_TO_RAGDOLL, ped, 5000, 5000, 0, true, true, false);

                if (allowMoveRagdoll) Game.Player.CanControlRagdoll = true;

                Vector3 forceVec = Vector3.Lerp(veh.ForwardVector, veh.UpVector, angle / 90.0f /* has to be from 0.0 to 1.0 */) * bailForce;

                ped.ApplyForce(forceVec);

                veh.ApplyForceRelative(new Vector3(0f, -(bailForce / 4), 0f)); //push back veh due to newton's third law

                Function.Call(Hash.PLAY_PAIN, ped, 8, 0, 0);

                if (playEjectSound) AudioPlaybackEngine.Instance.PlaySound(EJECT_SOUND_LOCATION);

                inAirAfterBail = true;
            }
            else
            {
                UI.Notify("Not able to eject passenger...", true);
            }
        }
    }
}
